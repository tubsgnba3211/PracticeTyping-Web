import sqlite3
import practice
from flask import Flask,  jsonify, json, request , render_template

app = Flask(__name__)


@app.route("/")
def hello():
    return render_template('home.html')


# 儲存練習結果
# 查詢練習結果 (全部)
@app.route("/result", methods=['GET', 'POST'])
def practice_result():
    if request.method == "GET":
        conn = sqlite3.connect('data.db')
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM `practice_result`;')
        records = cursor.fetchall()
        lstData = []
        for row in records:
            row_id, keyword, expect_time, actual_time, correct_time, error_time, correct_rate, total_time, speed_rate, start_time, end_time = row
            json = {}
            json["row_id"] = row_id
            json["keyword"] = keyword
            json["expect_time"] = expect_time
            json["actual_time"] = actual_time
            json["correct_time"] = correct_time
            json["error_time"] = error_time
            json["correct_rate"] = correct_rate
            json["total_time"] = total_time
            json["speed_rate"] = speed_rate
            json["start_time"] = start_time
            json["end_time"] = end_time
            lstData.append(json)
            print(json)
        data = jsonify(lstData)
        print(data)
        cursor.close()
        conn.close()
        return data
    elif request.method == "POST":
        conn = sqlite3.connect('data.db')
        cursor = conn.cursor()
        # To - Do : 接收前端資料
        keyword = "abcdefghijklmnopqrstuvwxyzc"
        expect_time = 3
        actual_time = 3
        correct_time = 2
        error_time = 1
        correct_rate = 66.67
        total_time = 38.76
        speed_rate = 12.92
        start_time = "2021-11-14 , 01:00:03"
        end_time = "2021-11-14 , 01:00:41"
        sql = """
        INSERT INTO practice_result
        ("keyword", "expect_time", "actual_time", "correct_time", "error_time",
        "correct_rate","total_time", "speed_rate", "start_time", "end_time")
        VALUES('{}' , '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')
        """.format(keyword, expect_time, actual_time, correct_time, error_time, correct_rate, total_time, speed_rate, start_time, end_time)
        print(sql)
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()
        return jsonify(result='OK')

# 刪除練習結果 (單筆)


@app.route("/result/<row_id>", methods=['DELETE'])
def deletePracticeResult(row_id):
    if request.method == "DELETE":
        conn = sqlite3.connect('data.db')
        cursor = conn.cursor()
        sql = "DELETE FROM practice_result where id={}".format(row_id)
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()
        return jsonify(result='OK')

# 查詢練習次數 (預設 100)
# 修改練習次數 (預設 100)


@app.route("/config/practice_time", methods=["GET", "POST"])
def config_practice_time():
    if request.method == "GET":
        configInfo = practice.loadConfig()
        practiceTime = int(configInfo["practice_time"])  # 100
        return jsonify(practice_time=practiceTime)
    elif request.method == "POST":
        practiceTime = request.form['practiceTime']
        practice.saveConfig(practiceTime)

        configInfo = practice.loadConfig()
        practiceTime = int(configInfo["practice_time"])  # 100
        return jsonify(practice_time=practiceTime)


# 查詢練習題目
# 新增練習題目
@app.route("/keywords", methods=["GET", "POST"])
def actionKeywords():
    if request.method == "GET":
        conn = sqlite3.connect('data.db')
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM `practice_keywords`;')
        records = cursor.fetchall()
        lstData = []
        for row in records:
            row_id, keyword = row
            json = {}
            json["row_id"] = row_id
            json["keyword"] = keyword
            lstData.append(json)
        data = jsonify(lstData)
        cursor.close()
        conn.close()
        return data
    elif request.method == "POST":
        conn = sqlite3.connect('data.db')
        cursor = conn.cursor()
        keyword = request.form['keyword']
        sql = """
        INSERT INTO practice_keywords ("keyword") VALUES('{}')
        """.format(keyword)
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()
        return jsonify(result="OK")


# 修改練習題目
# 刪除練習題目
@app.route("/keywords/<row_id>", methods=["PATCH", "DELETE"])
def modifyKeywords(row_id):
    if request.method == "PATCH":
        conn = sqlite3.connect('data.db')
        cursor = conn.cursor()
        keyword = request.form['keyword']
        sql = """
        UPDATE practice_keywords set keyword='{}' where id={}
        """.format(keyword, row_id)
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()
        return jsonify(result="OK")
    elif request.method == "DELETE":
        conn = sqlite3.connect('data.db')
        cursor = conn.cursor()
        sql = """
        DELETE FROM practice_keywords WHERE id = {}
        """.format(row_id)
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()
        return jsonify(result="OK")


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
