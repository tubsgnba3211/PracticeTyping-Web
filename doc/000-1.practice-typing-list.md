打字練習器 - 網頁版本 , 待辦清單
======


To Do - List
------
+ [X] create project - PracticeTyping-Web
+ [X] design : SPEC
	+ [X] 前端
	+ [X] 後端
	+ [X] 資料庫
+ [ ] dev : impl
	+ [X] 資料庫
	+ [X] 後端
	+ [ ] 前端
+ [ ] code : review


SPEC
------
前端 : 

	+ [ ] 過去的練習報告
		+ [ ] 查詢練習過的題目 ( Spanner )
		+ [ ] 依據題目查詢練習報告 ( New Api , Table)
		+ [ ] 依據 id 刪除練習報告 ( Button )
	+ [ ] 練習的參數設置 ( input + button)
	+ [ ] 練習的題目設置 
	+ [ ] 打字練習系統
		+ [ ] 開始練習
		+ [ ] 結束練習 (新增練習報告)

後端 : (API)
	
	+ [X] 儲存練習結果
	+ [X] 查詢練習結果 (全部)
	+ [X] 刪除練習結果 (單筆)
	+ [X] 查詢練習次數 (預設 100)
	+ [X] 修改練習次數 (預設 100)
	+ [X] 查詢練習題目
	+ [X] 新增練習題目
	+ [X] 修改練習題目
	+ [X] 刪除練習題目

資料庫 : (Table)

	+ [X] 練習結果 : practice_result
	+ [X] 練習題目 : practice_keywords




